# Menus: The Good, The Bad, and The Mega



## Introduction

Lee Quessenberry https://www.drupal.org/u/lquessenberry

Drupal Developer at Esteemed Inc.

Drupal Developer at RobertHalf

I am a self driven and motivated web developer / designer with a background in agriculture, construction, collegiate academics, and community development.



### Making A Choice:

- #### Manageability

  - Can a non-developer manage the structure?
  - Can a site builder manage the structure?
  - Can the content team / marketing manage the structure?
  - Who's ultimately responsible?
    - In our case (Protiviti/Vision) we (developers) are responsible.
    - In other cases it might be a small business owner or dedicated content team with varying degrees of skill.

- #### Usability

  - How deep does your nav need to be?
  - Is it easy to navigate?
  - Who's your audience?

- #### Translation

  - Is your menu available in Klingon?
  - What about Right To Left translations?

- #### Accessibility

  - Is it legible for colorblindness?
  - How about hearing impaired? 
  - Screen readers?

### Implementation Possibilities

- #### Twig With Entities
  - Menu Item Extras
    - Use nearly any entity
    - Layout Builder 
    - Blocks
    - Paragraphs
  - This is what we're using now for Protivi/Vision

- #### Roll Your Own

  - Custom Twig / With Drupal Core Menus
  - Static or Decoupled Custom Code 
    - If completely custom and without the use of Drupal core menus, you run into the risk of detrimentally compromising accessibility, manageability, and translation.
  - My Past Examples
    - https://www.metrolibrary.org/
    - https://www.handleyregional.org/
    - https://homeoutlet.com

- #### Contributed Modules

  - Pros
    - Convenient and high usability
  - Cons
    - Lots of extra CSS/Styling to override
  - https://www.drupal.org/project/simple_megamenu
  - https://www.drupal.org/project/tb_megamenu
  - https://www.drupal.org/project/we_megamenu

- #### Drupal Core Initiative - Decoupled Menus in Drupal (TBD)

  - https://www.youtube.com/watch?v=KPjMcZQu0k4
  - https://docs.google.com/document/d/1GPPALh7sQhRkV9dgm8qMde9RmwjYTteP_AE4K1NAfKI/edit#heading=h.jdqo4kat3mb2
  - https://dri.es/state-of-drupal-presentation-december-2020

### References

- https://drupal.stackexchange.com/questions/198648/any-easy-way-of-implementing-a-mega-menu
- https://www.ostraining.com/blog/drupal/megamenu-with-ultimenu-and-bootstrap/
- https://www.drupal.org/project/we_megamenu
- https://www.phase2technology.com/blog/creating-mega-menu-layout-builder
- https://www.drupal.org/project/simple_megamenu
- https://www.drupal.org/project/tb_megamenu
- https://www.drupal.org/project/we_megamenu
- https://www.droptica.com/blog/megamenu-drupal-example-droopler-distribution/

### Special Thanks

- :heart_eyes: **Krystal Quessenberry** for being patient with me.
- **:punch: Matt Glaman** for pushing me out of my comfort zone.
- :heavy_check_mark: **André Angelantoni** for believing in me.

- :construction_worker: **Esteemed** https://esteemed.io **(Drupal Contractors)**
- :business_suit_levitating: **Protiviti / Vision Team** (RobertHalf) https://protiviti.com 
- :books:**Library Market** https://librarymarket.com ​​
- :droplet: **Drupal / Drupal Initiative** https://drupal.org 
- :camping: **MidCamp** https://midcamp.org
- :shower: **E.C. Barton & Co.** https://homeoutlet.com
- :cow: **Centarro** https://centarro.io
- :woman_technologist: **Phase2** https://phase2technology.com

